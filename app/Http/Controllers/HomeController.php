<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(){
        return view('home');
    }

    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('success');
        } else {
            return back()->with('error','Login details are not valid');
        }
    }

    public function success(){
        return view('layouts.admin');
    }

    public function logout(){
        Auth::logout();
        return view('welcome');
    }   
}