<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('home/login', [App\Http\Controllers\HomeController::class, 'login'])->name('login');
Route::get('/home/success',[App\Http\Controllers\HomeController::class, 'success'])->name('success');
Route::get('/home/logout',[App\Http\Controllers\HomeController::class, 'logout'])->name('logout');



//Route::get('/home','HomeController@index')->name('home');